import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() childValue!: number;

  @Output() passToParent = new EventEmitter();

  value:number= 0
  constructor() { }

  ngOnInit(): void {
  }


  sendValue() {
    this.value ++
    this.passToParent.emit(this.value);

  }
  clearValue(){
    this.passToParent.emit(0);
  }

}
