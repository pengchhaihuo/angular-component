import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ChildComponent} from "../child/child.component";

@Component({
  selector: 'parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit,AfterViewInit {

  value!:number;
  valueChild:number =0


  constructor() { }

  @ViewChild(ChildComponent) child!: ChildComponent;

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.value =0
  }

  recievevalue(value:number){
    this.value = value;
  }

  sentDataToChild(){
   this.valueChild++
  }
  clearChildValue(){
    this.valueChild =0
  }
}
